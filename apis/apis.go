package apis

import (
	"fmt"
	"io/ioutil"
	"math"
	"net/http"
	"regexp"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/tidwall/gjson"
	"gitlab.com/adithyan-j/gotaskapp/apis/responses"
	h "gitlab.com/adithyan-j/gotaskapp/helpers"
	l "gitlab.com/adithyan-j/gotaskapp/logger"
	"gitlab.com/adithyan-j/gotaskapp/models"
)

func CreateRandomUser(c *gin.Context) {
	// get connection and log the DB
	// else Panic
	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Panicln("DB not GOT from Context")
	} else {
		l.InfoLogger.Println("CreateRandomUser()", db)
	}

	// GET randomuserapi
	// else return from CreateRandomUser() after returning JSON Response
	resp, err := http.Get("https://randomuser.me/api/?results=1")
	if err != nil {
		l.ErrorLogger.Println("GETing randomuserapi", err)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Error in GETing randomuserapi",
		}
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	} else {
		l.InfoLogger.Println("RandomUserApi:", resp.Status)
	}

	// read body of response to getData
	getData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		l.ErrorLogger.Println("Reading resp.Body of randomuserapi" + err.Error())
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Error in reading response body of randomuserapi",
		}
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}

	// get data from getData and prepare the random user
	var randomUser models.User
	randomUser.FirstName = gjson.GetBytes(getData, "results.0.name.first").String()
	randomUser.LastName = gjson.GetBytes(getData, "results.0.name.last").String()
	randomUser.Email = gjson.GetBytes(getData, "results.0.email").String()
	randomUser.UserType = "employee"
	l.InfoLogger.Println("Generated Randomuser: ", randomUser)

	validationResult := h.ValidateStruct(randomUser)
	// validate the prepared user input and insert it if valid
	if len(validationResult) > 0 {
		// log all the errors
		// return a JSON response of the errors
		// return from CreateUser()
		errorResponse := responses.ValidationErrorListResponse{
			Status:    "error",
			Message:   fmt.Sprint(len(validationResult), "errors found"),
			ErrorList: validationResult,
		}
		c.JSON(http.StatusInternalServerError, errorResponse)
	} else {
		// create password insert the prepared user
		passString := randomUser.FirstName + "employee"
		hash, _ := h.GeneratePassword(passString)
		randomUser.PasswordHash = hash
		_, err := db.Model(&randomUser).Insert()
		if err != nil {
			l.ErrorLogger.Println("Querying inserting randomUser", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "Error in executing the user creating query",
			}
			c.JSON(http.StatusInternalServerError, errorResponse)
		} else {
			l.InfoLogger.Println("User created:", randomUser.UserId)
			// fmt.Println(reflect.TypeOf(res))
			userCreatedResponse := responses.OneUserResponse{
				Status:  "success",
				Message: "user created",
				Result: models.UserWithoutPassword{
					UserId:    randomUser.UserId,
					FirstName: randomUser.FirstName,
					LastName:  randomUser.LastName,
					Email:     randomUser.Email,
					UserType:  randomUser.UserType,
				},
			}
			c.JSON(http.StatusCreated, userCreatedResponse)
		}
	}
}

func CreateUser(c *gin.Context) {
	// get connected and log the DB
	// else Panic
	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Panicln("DB not GOT from Context")
	} else {
		l.InfoLogger.Println("CreateUser()", db)
	}

	// read body from request to postData
	// on error return from CreateUser()
	postData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		l.ErrorLogger.Println("Reading createUser request.body", err)
		return
	} else {
		l.InfoLogger.Println("Raw postData:", postData)
	}

	// get data from postData
	// place them in user to validate before inserting in db
	var user models.User
	user.FirstName = gjson.GetBytes(postData, "first_name").String()
	user.LastName = gjson.GetBytes(postData, "last_name").String()
	user.Email = gjson.GetBytes(postData, "email").String()
	user.UserType = "employee"
	l.InfoLogger.Println("User to be inserted: ", user)

	// validate the prepared user input and insert it if valid
	validationResult := h.ValidateStruct(user)
	if len(validationResult) > 0 {
		// return a JSON response of the errors got from validation process
		// return from CreateUser()
		errorResponse := responses.ValidationErrorListResponse{
			Status:    "error",
			Message:   fmt.Sprint(len(validationResult), "errors found"),
			ErrorList: validationResult,
		}
		c.JSON(http.StatusBadRequest, errorResponse)
	} else {
		// generate passwrod and insert the prepared user
		passString := user.FirstName + "employee"
		hash, _ := h.GeneratePassword(passString)
		user.PasswordHash = hash

		_, err := db.Model(&user).Insert()
		if err != nil {
			l.ErrorLogger.Println("Executing insert user query", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "Error in executing the user creating query",
			}
			c.JSON(http.StatusInternalServerError, errorResponse)
		} else {
			l.InfoLogger.Println("User created:", user.UserId)
			// fmt.Println(reflect.TypeOf(res))
			userCreatedResponse := responses.OneUserResponse{
				Status:  "success",
				Message: "user created",
				Result: models.UserWithoutPassword{
					UserId:    user.UserId,
					FirstName: user.FirstName,
					LastName:  user.LastName,
					Email:     user.Email,
					UserType:  user.UserType,
				},
			}
			c.JSON(http.StatusCreated, userCreatedResponse)
		}
	}
}

func ListTargetUser(c *gin.Context) {

	// get connected and log the DB
	// if not connected Panic and close the program
	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Panicln("DB not GOT from Context")
	} else {
		l.InfoLogger.Println("ListTargetUser()", db)
	}

	// validate the userId
	// if invalid userId return from ListTargetUser()
	targetUserId := c.Param("userId")
	validUserId := regexp.MustCompile(`^[0-9]+$`)
	if !validUserId.MatchString(targetUserId) {
		l.ErrorLogger.Println("Invalid target userid:", targetUserId)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Invalid user_id",
		}
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	// execute the query and post the result
	var user models.User
	err := db.Model(&user).Where("user_id = ?", targetUserId).Select()
	if err != nil {
		if err == pg.ErrNoRows {
			l.ErrorLogger.Println("User not found", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "User not found in DB",
			}
			c.JSON(http.StatusBadRequest, errorResponse)
		} else {
			l.ErrorLogger.Println("Executing ListTagetUser ", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "Unexpected Error occured while executing ListTargetUser query",
			}
			c.JSON(http.StatusInternalServerError, errorResponse)

		}
	} else {
		l.InfoLogger.Println("User found:", targetUserId)
		newresponse := responses.OneUserResponse{
			Status:  "success",
			Message: "user found",
			Result: models.UserWithoutPassword{
				UserId:    user.UserId,
				FirstName: user.FirstName,
				LastName:  user.LastName,
				Email:     user.Email,
				UserType:  user.UserType,
			},
		}
		c.JSON(http.StatusOK, newresponse)
	}
}

func ListAllUsers(c *gin.Context) {

	// get connected and log the DB
	// if not connected Panic and close the program
	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Panicln("DB not GOT from Context")
	} else {
		l.InfoLogger.Println("ListAllUsers()", db)
	}

	// validate the request URL and check the page number if given
	requestUrl := c.Request.URL.String()

	validUrl := regexp.MustCompile(`^/user[/]?(\?page=\d*)?$`)
	// captures /user?page=11
	//	/user/?page=1
	//	/user
	//	/user/
	if !validUrl.MatchString(requestUrl) {
		l.ErrorLogger.Println("Invalid Request URL:", requestUrl)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Invalid Request URL",
		}
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	type user struct {
		UserId    int    `pg:"user_id" json:"user_id"`
		FirstName string `pg:"first_name" json:"first_name" validate:"required,alpha"`
		LastName  string `pg:"last_name" json:"last_name" validate:"required,alpha"`
		Email     string `pg:"email" json:"email" validate:"required,email"`
		UserType  string `pg:"user_type" json:"user_type" validate:"required,oneof=admin employee"`
	}

	// get pagenumber from url
	pageNumber, _ := strconv.Atoi(c.DefaultQuery("page", "0"))
	l.InfoLogger.Println("Page Number:", pageNumber)

	// get total number of items in db
	count, err := db.Model((*user)(nil)).Count()
	if err != nil {
		l.ErrorLogger.Println("Executing Count query", err)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Error getting total number of items in DB",
		}
		c.JSON(http.StatusInternalServerError, errorResponse)
	}

	// set total pages and offset
	limit := 5
	offset := 0
	totalPages := math.Ceil(float64(count) / float64(limit))
	if pageNumber == 0 {
		pageNumber = 1
	} else if pageNumber > int(totalPages) {
		offset = (int(totalPages) - 1) * limit
	} else {
		offset = (pageNumber - 1) * limit
	}

	// query all the users
	var userList []user
	err = db.Model(&userList).Limit(limit).Offset(offset).Select()
	if err != nil {
		l.ErrorLogger.Println("Executing ListALlUsers query", err)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Error in executing ListAllUsers query",
		}
		c.JSON(http.StatusInternalServerError, errorResponse)
	} else {
		l.InfoLogger.Println("Listing users")

		type ListAllUsersResponse struct {
			Status      string `json:"status"`
			Message     string `json:"message"`
			TotalResult string `json:"total_results"`
			CurrentPage string `json:"current_page"`
			TotalPages  string `json:"total_page"`
			Result      []user `json:"result"`
		}
		currentPage := fmt.Sprint(pageNumber)
		if pageNumber > int(totalPages) {
			currentPage = "Page limit exceeded! Displaying Last Page."
		}
		response := ListAllUsersResponse{
			Status:      "success",
			Message:     "listing users",
			TotalResult: fmt.Sprint(count),
			CurrentPage: currentPage,
			TotalPages:  fmt.Sprint(int(totalPages)),
			Result:      userList,
		}
		c.JSON(http.StatusOK, response)
	}
}

func DeleteUser(c *gin.Context) {
	// get connected
	// else Panic
	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Panicln("DB not GOT from context")
	} else {
		l.InfoLogger.Println("DeleteUser()", db)
	}

	// validate the userId
	// if invalid userId return from ListTargetUser()
	targetUserId := c.Param("userId")
	validUserId := regexp.MustCompile(`^[0-9]+$`)
	if !validUserId.MatchString(targetUserId) {
		l.ErrorLogger.Println("Invalid target userid:", targetUserId)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Invalid user_id",
		}
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	// get cookie and authorize it
	cookieAuthResult, httpStatusCode, message := h.GetAndAuthorzieCookie(c, "jwtToken")
	if httpStatusCode != 200 {
		errorResponse := responses.Response{
			Status:  "error",
			Message: message,
		}
		c.JSON(httpStatusCode, errorResponse)
		return
	} else if cookieAuthResult {
		l.InfoLogger.Println("Cookie authorized!, sub:", message)
	}

	// query the first user and post the response
	var user models.User
	res, err := db.Model(&user).Where("user_id = ?", targetUserId).Delete()
	// l.ErrorLogger.Println("DeleteUser res.RowsAffected():", res.RowsAffected())
	if err != nil {
		l.ErrorLogger.Println("Executing DeleteUser query", err)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Unexpected Error occured while executing DeleteUser query",
		}
		c.JSON(http.StatusInternalServerError, errorResponse)
	} else {
		if res.RowsAffected() > 0 {
			l.InfoLogger.Println("User Deleted, rowsAffected():", res.RowsAffected())
			newresponse := responses.Response{
				Status:  "success",
				Message: "user deleted",
			}
			c.JSON(http.StatusOK, newresponse)
		} else if res.RowsAffected() == 0 {
			l.ErrorLogger.Println("User not found in DB")
			errorResponse := responses.Response{
				Status:  "error",
				Message: "user not found in DB",
			}
			c.JSON(http.StatusBadRequest, errorResponse)
		}
	}
}

func UpdateUser(c *gin.Context) {
	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Panicln("DB not GOT from context")
	} else {
		l.InfoLogger.Println("UpdateUser()", db)
	}

	// validate the userId
	// if invalid userId return from ListTargetUser()
	targetUserId := c.Param("userId")
	validUserId := regexp.MustCompile(`^[0-9]+$`)
	if !validUserId.MatchString(targetUserId) {
		l.ErrorLogger.Println("Invalid target userid:", targetUserId)
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Invalid user_id",
		}
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	//get cookie and authorize it
	cookieAuthResult, httpStatusCode, message := h.GetAndAuthorzieCookie(c, "jwtToken")
	if httpStatusCode != 200 {
		errorResponse := responses.Response{
			Status:  "error",
			Message: message,
		}
		c.JSON(httpStatusCode, errorResponse)
		return
	} else if cookieAuthResult {
		l.InfoLogger.Println("Cookie authorized!, sub:", message)
	}

	// search for the user in db and retrieve data
	// place data in temp holder
	var temp models.User
	err := db.Model(&temp).Where("user_id = ?", targetUserId).Select()
	if err != nil {
		if err == pg.ErrNoRows {
			l.ErrorLogger.Println("User not found in db")
			errorResponse := responses.Response{
				Status:  "error",
				Message: "user not found in DB",
			}
			c.JSON(http.StatusBadRequest, errorResponse)
		} else {
			l.ErrorLogger.Println("Executing UpdateUser query", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "Unexpected Error occured searching for target in UpdateUser",
			}
			c.JSON(http.StatusBadRequest, errorResponse)
		}
		return
	} else {
		l.InfoLogger.Println("User found in DB")
	}

	// read body from request to postData
	// on error return from UpdateUser()
	postData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		l.ErrorLogger.Println("Reading createUser request.body", err)
		return
	} else {
		l.InfoLogger.Println("Raw postData:", postData)
	}

	//TODO: check for extra keys in input

	numberOfInput := 3
	if len(gjson.GetBytes(postData, "first_name").String()) > 0 {
		temp.FirstName = gjson.GetBytes(postData, "first_name").String()
	} else {
		numberOfInput--
	}
	if len(gjson.GetBytes(postData, "last_name").String()) > 0 {
		temp.LastName = gjson.GetBytes(postData, "last_name").String()
	} else {
		numberOfInput--
	}
	if len(gjson.GetBytes(postData, "email").String()) > 0 {
		temp.Email = gjson.GetBytes(postData, "email").String()
	} else {
		numberOfInput--
	}

	if numberOfInput == 0 {
		l.ErrorLogger.Println("Input cannot be empty/ Invalid input")
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Invalid Input",
		}
		c.JSON(http.StatusBadRequest, errorResponse)
		return
	}

	l.InfoLogger.Println("User to be inserted: ", temp)

	// validate the new info
	validationResult := h.ValidateStruct(temp)
	if len(validationResult) > 0 {
		// return a JSON response of the errors
		// return from CreateUser()
		errorResponse := responses.ValidationErrorListResponse{
			Status:    "error",
			Message:   fmt.Sprint(len(validationResult), " error found"),
			ErrorList: validationResult,
		}
		c.JSON(http.StatusBadRequest, errorResponse)
	} else {
		// insert the prepared user
		_, err := db.Model(&temp).Returning("user_id").Where("user_id = ?", targetUserId).Update()
		if err != nil {
			l.ErrorLogger.Println("Executing UpdaterUser query", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "Error in executing the user creating query",
			}
			c.JSON(http.StatusInternalServerError, errorResponse)
		} else {
			l.InfoLogger.Println("User updated:", temp.UserId)
			// fmt.Println(reflect.TypeOf(res))
			userCreatedResponse := responses.OneUserResponse{
				Status:  "success",
				Message: "user updated",
				Result: models.UserWithoutPassword{
					UserId:    temp.UserId,
					FirstName: temp.FirstName,
					LastName:  temp.LastName,
					Email:     temp.Email,
					UserType:  temp.UserType,
				},
			}
			c.JSON(http.StatusCreated, userCreatedResponse)
		}
	}
}

// for testing headers
func GetHeader(c *gin.Context) {
	// var xpsheader = c.Request.Header.Get("X-Nectar-is-nectar")
	l.InfoLogger.Println("ALL HEADERS: ", c.Request.Header)
	l.InfoLogger.Println("Content: ", c.Request.Header.Get("Content-Type"))
	l.InfoLogger.Println("XPS HEADER: ", c.Request.Header.Get("X-Nectar-is-nectar"))
	l.InfoLogger.Println("s HEADER: ", c.Request.Header.Get("s-Header"))
	l.InfoLogger.Println("Host: ", c.Request.Host)
	postData, _ := ioutil.ReadAll(c.Request.Body)
	l.InfoLogger.Println("Data: ", string(postData))

}
