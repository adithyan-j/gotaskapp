package apis

import (
	"io/ioutil"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/tidwall/gjson"
	"gitlab.com/adithyan-j/gotaskapp/apis/responses"
	h "gitlab.com/adithyan-j/gotaskapp/helpers"
	l "gitlab.com/adithyan-j/gotaskapp/logger"
	"gitlab.com/adithyan-j/gotaskapp/models"
)

func Login(c *gin.Context) {

	type credentials struct {
		email    string
		password string
	}

	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Panicln("DB not GOT from context")
	} else {
		l.InfoLogger.Println("Login()", db)
	}

	// read body of response to getData
	getData, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		l.ErrorLogger.Println("Reading request.Body of Login" + err.Error())
		errorResponse := responses.Response{
			Status:  "error",
			Message: "Error in reading response body of randomuserapi",
		}
		c.JSON(http.StatusInternalServerError, errorResponse)
		return
	}

	// get email and passwords from the getData
	var cred credentials
	cred.email = gjson.GetBytes(getData, "email").String()
	// l.InfoLogger.Println(cred.email)
	cred.password = gjson.GetBytes(getData, "password").String()
	// l.InfoLogger.Println(cred.password)

	var user models.User
	err = db.Model(&user).Where("email = ?", cred.email).Select()
	// l.InfoLogger.Println("User got from DB", user)
	if err != nil {
		if err == pg.ErrNoRows {
			l.ErrorLogger.Println("User not found in db")
			errorResponse := responses.Response{
				Status:  "error",
				Message: "User not found in DB",
			}
			c.JSON(http.StatusNotFound, errorResponse)
		} else {
			l.ErrorLogger.Println("Executing finding user in DB ", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "Unexpected Error occured while executing FindingUser query",
			}
			c.JSON(http.StatusInternalServerError, errorResponse)
		}
		return
	} else {
		// if login successful genearte and set JWT in cookie
		loginSuccess, _ := h.ComparePassword(cred.password, user.PasswordHash)
		if loginSuccess {
			tokenString := ""
			tokenString, err := h.GenerateJWT(user.Email)
			if err == nil {
				l.InfoLogger.Println("Generated JWT:", tokenString)
				successResponse := responses.ResponseWithResult{
					Status:  "success",
					Message: "Login successful",
					Result:  tokenString,
				}
				expTime := time.Now().Add(time.Minute * 5).Unix()
				c.SetCookie("jwtToken", tokenString, int(expTime), "/user", "localhost", false, true)
				c.JSON(http.StatusOK, successResponse)
			}
			l.InfoLogger.Println("User: " + user.Email + " logined!!")
		} else {
			l.ErrorLogger.Println("Login failed", err)
			errorResponse := responses.Response{
				Status:  "error",
				Message: "Given Password is wrong",
			}
			c.JSON(http.StatusBadRequest, errorResponse)
			return
		}
	}
}

func Logout(c *gin.Context) {

	token, err := c.Request.Cookie("jwtToken")
	if err != nil {
		if err == http.ErrNoCookie {
			l.ErrorLogger.Println("JwtToken cookie not found")
			errorResponse := responses.Response{
				Status:  "error",
				Message: "user is not logged in",
			}
			c.JSON(http.StatusBadRequest, errorResponse)
		} else {
			l.ErrorLogger.Println("JwtToken cookie not found:", err.Error())
			errorResponse := responses.Response{
				Status:  "error",
				Message: "user is not logged in",
			}
			c.JSON(http.StatusBadRequest, errorResponse)
		}
		return
	} else {
		l.InfoLogger.Println("Cookie found, token:", token)
		c.SetCookie("jwtToken", "deletedCookie", -1, "/user", "localhost", false, true)
		l.InfoLogger.Println("Cookie Deleted:", token.Name, "Expires:", token.Expires.String())
		successResponse := responses.Response{
			Status:  "success",
			Message: "user logged out",
		}
		c.JSON(http.StatusBadRequest, successResponse)
		return
	}
}
