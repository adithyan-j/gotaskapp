package responses

import "gitlab.com/adithyan-j/gotaskapp/models"

type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

type ResponseWithResult struct {
	Status  string `json:"status"`
	Message string `json:"message"`
	Result  string `json:"result"`
}

type ValidationErrorListResponse struct {
	Status    string   `json:"status"`
	Message   string   `json:"message"`
	ErrorList []string `json:"error_list"`
}

type OneUserResponse struct {
	Status  string                     `json:"status"`
	Message string                     `json:"message"`
	Result  models.UserWithoutPassword `json:"result"`
}

type UpdateUserResponse struct {
	Status  string            `json:"status"`
	Message string            `json:"message"`
	Result  models.UpdateUser `json:"result"`
}
