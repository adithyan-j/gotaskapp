package models

// CREATE TABLE public.users (
// 	user_id serial NOT NULL,
// 	first_name varchar(100) NOT NULL,
// 	last_name varchar(100) NOT NULL,
// 	email varchar(100) NOT NULL,
// 	CONSTRAINT users_email_key UNIQUE (email),
// 	CONSTRAINT users_pkey PRIMARY KEY (user_id)
// );

type User struct {
	UserId       int    `pg:"user_id" json:"user_id"`
	FirstName    string `pg:"first_name" json:"first_name" validate:"required,alpha"`
	LastName     string `pg:"last_name" json:"last_name" validate:"required,alpha"`
	Email        string `pg:"email" json:"email" validate:"required,email"`
	PasswordHash string `pg:"password_hash"`
	UserType     string `pg:"user_type" json:"user_type" validate:"required,oneof=admin employee"`
}

type UserWithoutPassword struct {
	tableName struct{} `pg:"users"`
	UserId    int      `pg:"user_id" json:"user_id"`
	FirstName string   `pg:"first_name" json:"first_name" validate:"required,alpha"`
	LastName  string   `pg:"last_name" json:"last_name" validate:"required,alpha"`
	Email     string   `pg:"email" json:"email" validate:"required,email"`
	UserType  string   `pg:"user_type" json:"user_type" validate:"required,oneof=admin employee"`
}

type UpdateUser struct {
	// tableName struct{} `pg:"users"`
	UserId    int    `pg:"user_id" json:"user_id"`
	FirstName string `pg:"first_name" json:"first_name" validate:"alpha"`
	LastName  string `pg:"last_name" json:"last_name" validate:"alpha"`
	Email     string `pg:"email" json:"email" validate:"email"`
}

// CREATE TABLE public.bankdetails (
// 	bank_id serial NOT NULL,
// 	user_id serial NOT NULL,
// 	bank_name varchar(100) NOT NULL,
// 	CONSTRAINT bankdetails_pkey PRIMARY KEY (bank_id),
// 	CONSTRAINT fk_userid FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON DELETE CASCADE
// );
