package main

import (
	"fmt"

	"github.com/golang-jwt/jwt"
)

func ValidateJWT(token string) (string, error) {

	// key := []byte("somekey")

	// tempClaims := jwt.MapClaims{}
	// tempClaims["admin"] = false
	// tempClaims["sub"] = "admin.com"
	// tempClaims["exp"] = time.Now().Add(time.Minute * 25).Unix()

	// genToken := jwt.NewWithClaims(jwt.SigningMethodHS256, tempClaims)
	// fmt.Println("rawTOKEN:", genToken.Claims)
	// fmt.Println("rawTOKEN:", genToken.Header)
	// fmt.Println("rawTOKEN:", genToken.Signature)
	// fmt.Println("rawTOKEN:", genToken.Claims)

	// tokenString, err := genToken.SignedString(key)
	// if err != nil {
	// 	fmt.Println("ERROR IN GEN:", err)
	// } else {
	// 	fmt.Println("TOKENstring:", tokenString)
	// }

	validToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		return []byte("somekey"), nil
	})
	if err != nil {
		// var temp map[string]interface{}
		temp := validToken.Claims.(jwt.MapClaims)
		fmt.Println("header", validToken.Header)
		fmt.Println("claims", temp["sub"])
		// fmt.Println("claims[subn]", validToken.Claims["sub"])
		fmt.Println("signature", validToken.Signature)
		return "", err
	} else {
		fmt.Println("header", validToken.Header)
		fmt.Println("claims", validToken.Claims)
		fmt.Println("signature", validToken.Signature)
		return string(validToken.Raw), nil
	}
}

func main() {
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhZG1pbiI6ZmFsc2UsImV4cCI6MTYzMDM4NTExNSwic3ViIjoiYWRtaW4uY29tIn0.2fmmkIp9-wzZA_JZn4A7H-bmDa1Zt1ZFnVWHncm9CUg"
	result, err := ValidateJWT(token)
	if err != nil {
		fmt.Println(result, err)
	} else {
		fmt.Println("valid", result)
	}
}
