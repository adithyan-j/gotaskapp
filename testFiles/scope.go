package main

import (
	"encoding/json"
	"fmt"

	"github.com/tidwall/gjson"
)

var a string = "a var"

func init() {
	// var a string = "internal a"
	fmt.Println("Scope testing")
	fmt.Println(a)

	type User struct {
		// tableName struct{} `pg:"users"`
		FirstName string `json:"firstName"`
		LastName  string `json:"lastName"`
		Email     string `json:"email"`
	}

	type SuccessResponse struct {
		Status  string `json:"status"`
		Message User   `json:"message"`
	}

	emp := User{
		FirstName: "firstname",
		LastName:  "lastname",
		Email:     "soemthin@something.com",
	}

	tempResponse := SuccessResponse{
		Status:  "success",
		Message: emp,
	}

	payload, err := json.Marshal(tempResponse)
	if err != nil {
		fmt.Println("Error while marshalling")
	} else {
		fmt.Println(gjson.ParseBytes(payload))
	}
}
