package main

import (
	"fmt"

	"github.com/go-playground/validator/v10"
)

type User struct {
	FirstName string `validate:"required"`
	LastName  string `validate:"required"`
	Age       uint8  `validate:"gte=0,lte=130"`
	Email     string `validate:"required,email"`
}

func main() {

	validate := validator.New()

	user := &User{
		FirstName: "Badger",
		LastName:  "Smith",
		Age:       135,
		Email:     "Badger.Smithgmail.com",
	}

	// emptyUSer := &User{}

	err := validate.Struct(user)
	if err != nil {

		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		// if _, ok := err.(*validator.InvalidValidationError); !ok {
		// 	fmt.Println(err)
		// 	return
		// }

		for _, err := range err.(validator.ValidationErrors) {

			fmt.Println("err namespace", err.Namespace())
			fmt.Println("field", err.Field()) // field Email
			fmt.Println("value", err.Value()) //value <value of the err'd field>
			// fmt.Println("struct namespace", err.StructNamespace())
			// fmt.Println("struct field", err.StructField())
			// fmt.Println("tag", err.Tag())
			// fmt.Println("actual tag", err.ActualTag())
			// fmt.Println("kind", err.Kind())
			// fmt.Println("type", err.Type())
			// fmt.Println("param", err.Param())
			fmt.Println()
		}

		// from here you can create your own error messages in whatever language you wish
		return

	} else {
		fmt.Println("No Error in input")
	}

}
