package main

import (
	"fmt"

	"github.com/go-pg/pg"
)

func main() {
	address := fmt.Sprintf("%s:%s", "localhost", "5432")

	db := pg.Connect(&pg.Options{
		User:     "postgres",
		Password: "password",
		Addr:     address,
		Database: "company",
	})

	type user struct {
		UserId    int    `pg:"user_id" json:"user_id"`
		FirstName string `pg:"first_name" json:"first_name" validate:"required,alpha"`
		LastName  string `pg:"last_name" json:"last_name" validate:"required,alpha"`
		Email     string `pg:"email" json:"email" validate:"required,email"`
		UserType  string `pg:"user_type" json:"user_type" validate:"required,oneof=admin employee"`
	}

	var userList []user
	err := db.Model(&userList).Limit(5).Offset(3).Select()
	if err != nil {
		fmt.Println("Executing ListALlUsers query", err)
	} else {
		fmt.Println("Listing users")
		fmt.Println(userList)
	}

	// hash, _ := helpers.GeneratePassword("adminthreeadmin")

	// user = models.User{
	// 	FirstName:    "admin",
	// 	LastName:     "three",
	// 	Email:        "adminthree@admin.com",
	// 	PasswordHash: hash,
	// 	UserType:     "admin",
	// }

	// _, err := db.Model(&user)..Insert()
	// fmt.Println(user.Hash)
	// fmt.Println(user.UserId)
	// if err == nil {

	// 	fmt.Println(user)
	// }

}
