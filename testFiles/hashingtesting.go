package main

import (
	"fmt"

	"gitlab.com/adithyan-j/gotaskapp/helpers"
)

type PasswordConfig struct {
	time    uint32
	memory  uint32
	threads uint8
	keyLen  uint32
}

func main() {
	// salt := []byte("some salt")

	// key1 := argon2.Key([]byte("some password"), salt, 3, 32*1024, 4, 32)
	// fmt.Println(key1)

	// key2 := argon2.Key([]byte("some password"), salt, 3, 32*1024, 4, 32)
	// fmt.Println(key2)

	// var key3 []byte
	// key3 = []byte{14, 21, 247, 233, 95, 215, 154, 188, 114, 50, 70, 202, 131, 24, 49, 11, 24, 182, 110, 177, 196, 242, 32, 200, 142, 223, 13, 207, 250, 5, 2, 81}

	// if subtle.ConstantTimeCompare(key1, key3) == 1 {
	// 	fmt.Println("YES")
	// } else {
	// 	fmt.Println("NO")
	// }

	// hash, err := helpers.GeneratePassword("admintwoadmin")
	// if err == nil {
	// 	fmt.Println(hash)
	// }

	hash := "$argon2id$v=19$m=65536,t=1,p=4$Kqgc2bTrnYwoiVuvq4NixQ$FyLH+sTDqpGpFZGnPztTEPZCL6zE8YrkN06om4hm6Ho"
	match, err := helpers.ComparePassword("Emmanuelemployee", hash)
	if !match || err != nil {
		fmt.Println("Password Invalid")
	} else {
		fmt.Println("Password Valid")
	}
}
