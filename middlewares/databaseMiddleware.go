package middlewares

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	l "gitlab.com/adithyan-j/gotaskapp/logger"
)

func Database() gin.HandlerFunc {
	address := fmt.Sprintf("%s:%s", "localhost", "5432")

	// connect to the db and initialise the connection in var db
	db := pg.Connect(&pg.Options{
		User:     "postgres",
		Password: "password",
		Addr:     address,
		Database: "company",
	})

	if db == nil {
		l.ErrorLogger.Println("Cannot connect to db.")
	}

	// set db in the gin context
	return func(c *gin.Context) {
		c.Set("db", db)
		c.Next()
	}
}
