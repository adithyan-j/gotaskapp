package logger

import (
	"log"
	"os"
)

var (
	InfoLogger    *log.Logger
	ErrorLogger   *log.Logger
	WarningLogger *log.Logger
)

func init() {

	InfoLogger = log.New(os.Stdout, "INFO: ", log.Lshortfile)
	ErrorLogger = log.New(os.Stdout, "ERROR: ", log.Lshortfile)
	WarningLogger = log.New(os.Stdout, "WARNING: ", log.Lshortfile)
}
