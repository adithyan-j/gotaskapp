module gitlab.com/adithyan-j/gotaskapp

go 1.16

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.3 // indirect
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-pg/pg/v10 v10.10.3
	github.com/go-playground/validator/v10 v10.9.0 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible // indirect
	github.com/lib/pq v1.10.2 // indirect
	github.com/matthewhartstonge/argon2 v0.1.5 // indirect
	github.com/tidwall/gjson v1.8.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5 // indirect
)
