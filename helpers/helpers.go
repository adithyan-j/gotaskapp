package helpers

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	l "gitlab.com/adithyan-j/gotaskapp/logger"
	"gitlab.com/adithyan-j/gotaskapp/models"
)

func ValidateStruct(inputUser models.User) []string {
	validate := validator.New()
	errorList := []string{}
	// validate the prepared user input and insert it if valid
	err := validate.Struct(inputUser)
	if err != nil {
		// log all the errors
		// prepare a list the errors
		for _, err := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprint("ValidationError at Field(", err.Field(), ") for value(", err.Value(), ")")
			l.ErrorLogger.Println(errorMessage)
			errorList = append(errorList, errorMessage)
		}
	}
	return errorList
}

func GetAndAuthorzieCookie(c *gin.Context, inputCookie string) (bool, int, string) {
	var boolValue bool = true
	var intValue int = http.StatusOK
	var stringValue string = ""
	cookiePresent, err := c.Request.Cookie(inputCookie)
	if err != nil {
		l.ErrorLogger.Println("Getting cookie:", err.Error())
		errorMessage := err.Error()
		errorCode := http.StatusBadRequest
		if err == http.ErrNoCookie {
			errorMessage = "user is not logged in"
			errorCode = http.StatusUnauthorized
		}
		boolValue, intValue, stringValue = false, errorCode, errorMessage
	} else {
		l.InfoLogger.Println("Cookie present:", cookiePresent)
		isAuthorizedUser, jwtSub, err := AuthorizeJWT(c, cookiePresent.Value)
		if err != nil {
			l.ErrorLogger.Println("validating JWT", err.Error())
			errorCode := http.StatusBadRequest
			errorMessage := err.Error()
			if err.Error() == "Token is expired" {
				errorMessage = "Login expired."
			}
			boolValue, intValue, stringValue = false, errorCode, errorMessage
		} else if !isAuthorizedUser {
			boolValue, intValue, stringValue = false, http.StatusUnauthorized, "unauthorized user"
		} else if isAuthorizedUser {
			boolValue, intValue, stringValue = true, http.StatusOK, jwtSub
		}
	}
	return boolValue, intValue, stringValue
}
