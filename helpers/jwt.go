package helpers

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/go-pg/pg"
	"github.com/golang-jwt/jwt"
	l "gitlab.com/adithyan-j/gotaskapp/logger"
	"gitlab.com/adithyan-j/gotaskapp/models"
)

//qwerty
//12345
func GenerateJWT(inputEmail string) (string, error) {

	key := []byte("some_key")

	tempClaims := jwt.MapClaims{}
	tempClaims["admin"] = true
	tempClaims["sub"] = inputEmail
	l.InfoLogger.Println("GenerateJWT for user:", inputEmail)
	tempClaims["exp"] = time.Now().Add(time.Minute * 5).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, tempClaims)

	tokenString, err := token.SignedString(key)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func AuthorizeJWT(c *gin.Context, inputToken string) (bool, string, error) {
	db := c.MustGet("db").(*pg.DB)
	if db == nil {
		l.ErrorLogger.Println("Error ")
	} else {
		l.InfoLogger.Println("ValidateJWT()")
	}

	parsedToken, err := jwt.Parse(inputToken, func(token *jwt.Token) (interface{}, error) {
		return []byte("some_key"), nil
	})

	if err != nil {
		return false, "", err
	} else {
		var temp models.User
		parsedTokenClaims := parsedToken.Claims.(jwt.MapClaims)
		// return parsedTokenClaims["sub"].(string), nil
		wantedSub := parsedTokenClaims["sub"].(string)
		err := db.Model(&temp).Where("email = ?", wantedSub).Select()
		if err != nil {
			return false, "", err
		} else {
			if temp.UserType != "admin" {
				l.ErrorLogger.Println(temp.Email, "user is not admin")
				return false, "", nil
			} else {
				l.InfoLogger.Println("Authorized user:", temp.Email)
				return true, temp.Email, nil
			}
		}
	}
}
