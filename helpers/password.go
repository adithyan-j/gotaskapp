package helpers

import (
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"fmt"
	"strings"

	"golang.org/x/crypto/argon2"
)

type passwordConfig struct {
	time    uint32
	memory  uint32
	threads uint8
	keyLen  uint32
}

func GeneratePassword(password string) (string, error) {

	config := &passwordConfig{
		time:    1,
		memory:  64 * 1024,
		threads: 4,
		keyLen:  32,
	}

	// Generate a random Salt
	salt := make([]byte, 16)
	if _, err := rand.Read(salt); err != nil {
		return "", err
	}

	key := argon2.IDKey([]byte(password), salt, config.time, config.memory, config.threads, config.keyLen)

	// Base64 encode the salt and hashed password.
	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	b64key := base64.RawStdEncoding.EncodeToString(key)

	format := "$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s"
	//$argon2id$v=19$m=65536,t=1,p=4$LaPbZo7f3nViqRA6EMLgfA$LWOCXaoG+VxcTdmVClz20nWZGrxWcEEJsASzpGDfIiM
	hash := fmt.Sprintf(format, argon2.Version, config.memory, config.time, config.threads, b64Salt, b64key)
	return hash, nil
}

func ComparePassword(inputPassword, hash string) (bool, error) {

	parts := strings.Split(hash, "$")

	c := &passwordConfig{}
	_, err := fmt.Sscanf(parts[3], "m=%d,t=%d,p=%d", &c.memory, &c.time, &c.threads)
	if err != nil {
		return false, err
	}

	salt, err := base64.RawStdEncoding.DecodeString(parts[4])
	if err != nil {
		return false, err
	}

	decodedHash, err := base64.RawStdEncoding.DecodeString(parts[5])
	if err != nil {
		return false, err
	}

	c.keyLen = uint32(len(decodedHash))

	comparingHash := argon2.IDKey([]byte(inputPassword), salt, c.time, c.memory, c.threads, c.keyLen)

	return (subtle.ConstantTimeCompare(decodedHash, comparingHash) == 1), nil
}
