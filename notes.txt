CREATE TABLE public.users (
	user_id serial NOT NULL,
	first_name varchar(100) NOT NULL,
	last_name varchar(100) NOT NULL,
	email varchar(100) NOT NULL,
	CONSTRAINT users_email_key UNIQUE (email),
	CONSTRAINT users_pkey PRIMARY KEY (user_id)
);


CREATE TABLE public.bankdetails (
	bank_id serial NOT NULL,
	user_id serial NOT NULL,
	bank_name varchar(100) NOT NULL,
	CONSTRAINT bankdetails_pkey PRIMARY KEY (bank_id),
	CONSTRAINT fk_userid FOREIGN KEY (user_id) REFERENCES public.users(user_id) ON DELETE CASCADE
);
