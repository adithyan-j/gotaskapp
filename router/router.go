package router

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/adithyan-j/gotaskapp/apis"
	"gitlab.com/adithyan-j/gotaskapp/middlewares"
)

var r = gin.Default()

func Start() {
	mainRouter()
	r.Run("localhost:5234")
}

func mainRouter() *gin.Engine {

	r.Use(middlewares.Database())
	// r.Use(cors.Default())
	config := cors.DefaultConfig()
	config.AllowAllOrigins = true
	config.AllowMethods = []string{"GET", "PUT", "POST"}
	config.AllowHeaders = []string{"*"}
	r.Use(cors.New(config))

	r.GET("/home", func(c *gin.Context) {
		c.JSON(http.StatusAccepted, gin.H{
			"msg": "Pinged Home Page",
		})
	})

	// login service endpoints
	r.GET("/user/login", apis.Login)
	r.POST("/user/logout", apis.Logout)

	// user creating endpoints
	r.POST("/user/random", apis.CreateRandomUser)
	r.POST("/user", apis.CreateUser)

	// user listing endpoints
	r.GET("/user/:userId", apis.ListTargetUser)
	r.GET("/user", apis.ListAllUsers)

	// admin only endpoints
	r.PUT("/user/:userId", apis.UpdateUser)
	r.DELETE("/user/:userId", apis.DeleteUser)

	// FOR TESTING HEADERS
	r.PUT("/getheader", apis.GetHeader)
	r.POST("/getheader", apis.GetHeader)

	return r
}
