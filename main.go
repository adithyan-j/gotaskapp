package main

import (
	router "gitlab.com/adithyan-j/gotaskapp/router"
)

func main() {

	// start a new connection
	// router.NewDBConn()
	// start the router
	router.Start()
}
